﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SharpDX.XInput;
using System.IO.Ports;

namespace ControllerSerialLink
{
    public partial class MainWindow : Form
    {
        private CheckBox[] connectedPadBoxes;
        private RadioButton[] selectedPadBoxes;

        private GameControllers gamepads = new GameControllers(10);
        private int selectedPad = 0;
        private bool serialConnected = false;

        /// <summary>
        /// Handles user inputs and updating output values in the UI.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            // Set up arrays of gamepad controls in the UI and hook up event handler to radio buttons
            connectedPadBoxes = new CheckBox[] { pad1ConnBox, pad2ConnBox, pad3ConnBox, pad4ConnBox };
            selectedPadBoxes = new RadioButton[] { pad1Sel, pad2Sel, pad3Sel, pad4Sel };
            foreach (RadioButton box in selectedPadBoxes) box.Click += new EventHandler(GamepadRadio_Click);

            fillSerialOptions();
            updateGamepadStatus();

            gamepads.ConnectionChanged += GameControllers_ConnectionChanged;
            gamepads.InputChanged += GameControllers_InputChanged;
            gamepads.EnablePolling(true);
        }

        /// <summary>
        /// Delegate for the Game Controller Connection Changed Callback. Generally sets Controller Status indicators.
        /// </summary>
        private delegate void onlineControllerSetter(object sender, GameControllers.ConnectionEventArgs e);
        /// <summary>
        /// Event Handler for Game Controller Connection Changed events. Fires when a controller connects or disconnects.
        /// </summary>
        /// <param name="sender">The Game Controller that changed</param>
        /// <param name="e">Game Controller ConnectionEventArgs specifying whether the controller is Enabled or not</param>
        private void GameControllers_ConnectionChanged(object sender, GameControllers.ConnectionEventArgs e)
        {
            var controller = (GameControllers.GameController)sender;
            var changedPad = (int)controller.controllerIdx;
            ref var changedPadCheckBox = ref connectedPadBoxes[changedPad];
            if (changedPadCheckBox.InvokeRequired)
            {
                var d = new onlineControllerSetter(GameControllers_ConnectionChanged);
                changedPadCheckBox.Invoke(d, new object[] { sender, e });
            }
            else
            {
                if (changedPad == selectedPad) updateGamepadStatus();
                changedPadCheckBox.Checked = e.Enabled;
                eventLogBox.AppendText(
                    String.Format(
                        "[{1}] Controller #{2} has been {3}.{0}",
                        System.Environment.NewLine, System.DateTime.Now,
                        changedPad + 1, 
                        e.Enabled ? "connected" : "disconnected"));
            }
        }

        /// <summary>
        /// Delegate for the Game Controller Input Changed Callback. Generally sets entries in the event log.
        /// </summary>
        private delegate void eventLogSetter(object sender, GameControllers.InputEventArgs e);
        /// <summary>
        /// Event Handler for Game Controller Input Changed events. Fires when a controller pad changes.
        /// </summary>
        /// <param name="sender">The Game Controller that changed</param>
        /// <param name="e">Game Controller InputEventArgs specifying the Controller Pad State</param>
        private void GameControllers_InputChanged(object sender, GameControllers.InputEventArgs e)
        {
            var controller = (GameControllers.GameController)sender;
            var changedPad = (int)controller.controllerIdx;
            if (eventLogBox.InvokeRequired)
            {
                var d = new eventLogSetter(GameControllers_InputChanged);
                eventLogBox.Invoke(d, new object[] { sender, e });
            }
            else
            {
                eventLogBox.AppendText(
                    String.Format(
                        "[{1}] Controller #{2}:{0}{3}{0}",
                        System.Environment.NewLine, System.DateTime.Now,
                        changedPad + 1,
                        e.State.Gamepad));
                eventLogBox.SelectionStart = eventLogBox.TextLength;
                eventLogBox.ScrollToCaret();
                if (serialConnected)
                {
                    if (!e.State.Gamepad.Buttons.HasFlag(GamepadButtonFlags.DPadLeft) &&
                        !e.State.Gamepad.Buttons.HasFlag(GamepadButtonFlags.DPadRight))
                        serialPort.Write("Stop\r\n"); 
                    if (e.State.Gamepad.Buttons.HasFlag(GamepadButtonFlags.DPadLeft) &&
                        !controller.lastState.Gamepad.Buttons.HasFlag(GamepadButtonFlags.DPadLeft))
                        serialPort.Write("CounterClockwise\r\n");
                    if (e.State.Gamepad.Buttons.HasFlag(GamepadButtonFlags.DPadRight) &&
                        !controller.lastState.Gamepad.Buttons.HasFlag(GamepadButtonFlags.DPadRight))
                        serialPort.Write("Clockwise\r\n");
                }
            }
        }

        /// <summary>
        /// Populate the serial options dropdown boxes.
        /// </summary>
        private void fillSerialOptions()
        {
            String[] comPorts = SerialPort.GetPortNames();
            comPorts = comPorts.OrderBy(
                p => int.Parse(p.Trim(new char[]{ 'C', 'O', 'M' }))
                ).ToArray();
            comPortList.DataSource = comPorts;
            var selectedIdx = comPortList.FindStringExact(Properties.Settings.Default.comPort);
            if (selectedIdx == ListBox.NoMatches)
            {
                MessageBox.Show("Your preferred serial port is not available on the system," +
                    "selecting the first serial port.",
                    "Serial Port Unavilable");
                selectedIdx = 0;
            }
            comPortList.SelectedIndex = selectedIdx;

            parityInput.DataSource = new ParitySource();
            parityInput.DisplayMember = "name";
            parityInput.ValueMember = "value";

            flowControlInput.DataSource = new FlowControlSource();
            flowControlInput.DisplayMember = "name";
            flowControlInput.ValueMember = "value";

            int[] dataBits = Enumerable.Range(5, 5).ToArray();
            dataBitsInput.DataSource = dataBits;

            StopBits[] stopBits = { StopBits.One, StopBits.Two };
            stopBitsInput.DataSource = stopBits;
        }

        /// <summary>
        /// Updates the status of the UI elements on the Controller Information pane.
        /// </summary>
        private void updateGamepadStatus()
        {
            Controller gamepad = gamepads.GetPadController(selectedPad);
            selectedPadLabel.Text = "Controller " + (selectedPad + 1);
            if (gamepad.IsConnected)
            {
                Capabilities caps = gamepad.GetCapabilities(DeviceQueryType.Gamepad);
                BatteryInformation bat = gamepad.GetBatteryInformation(BatteryDeviceType.Gamepad);
                padConnectedLabel.Text = "Connected";
                batTypeLabel.Text = bat.BatteryType.ToString();
                batLevelLabel.Text = bat.BatteryLevel.ToString();
                capTypeLabel.Text = caps.Type.ToString() + " / " + caps.SubType.ToString();
                capFlagLabel.Text = caps.Flags.ToString();
                String[] buttons = caps.Gamepad.Buttons.ToString().Split(',');
                buttons = Array.ConvertAll(buttons, x => x.Trim(' '));
                buttonListBox.DataSource = buttons;
            }
            else
            {
                padConnectedLabel.Text = "Not Connected";
                batTypeLabel.Text = "N/A";
                batLevelLabel.Text = "N/A";
                capTypeLabel.Text = "N/A";
                capFlagLabel.Text = "N/A";
                String[] naSource = { "N/A" };
                buttonListBox.DataSource = naSource;
            }
        }

        /// <summary>
        /// Connects or disconnects to the Serial Port.
        /// </summary>
        private void ConnectionButton_Click(object sender, EventArgs e)
        {
            if (!serialConnected)
            {
                serialPort.PortName = comPortList.GetItemText(comPortList.SelectedItem);
                serialPort.BaudRate = int.Parse(baudInput.Text);
                serialPort.Parity = (Parity)parityInput.SelectedValue;
                serialPort.Handshake = (Handshake)flowControlInput.SelectedValue;
                serialPort.DataBits = (int)dataBitsInput.SelectedValue;
                serialPort.StopBits = (StopBits)stopBitsInput.SelectedValue;
                if (!serialPort.IsOpen)
                {
                    serialPort.Open();
                    serialConnected = true;
                    connectionButton.Text = "Disconnect Serial";
                } else
                {
                    MessageBox.Show("Warning!", "The serial port selected is currently in use!");
                }
            }
            else
            {
                serialPort.Close();
                serialConnected = false;
                connectionButton.Text = "Connect Serial";
            }
        }

        /// <summary>
        /// Event Handler for Gamepad Radio Button modifications.
        /// </summary>
        private void GamepadRadio_Click(object sender, EventArgs e)
        {
            var lastSelectedPad = selectedPad;
            selectedPad = (int)((RadioButton)sender).Tag;
            if (selectedPad != lastSelectedPad) updateGamepadStatus();
            var controller = gamepads.GetPadController(selectedPad);
            if (vibeSelectedBox.Checked && controller.IsConnected)
                gamepads.VibratePad(
                    controller,
                    (int)vibePadTimeBox.Value,
                    (ushort)(leftVibeFreqBox.Value * (ushort.MaxValue / leftVibeFreqBox.Maximum)),
                    (ushort)(rightVibeFreqBox.Value * (ushort.MaxValue / rightVibeFreqBox.Maximum))
                );
        }

        /// <summary>
        /// Event Handler for Auto-Refresh interval modifications.
        /// </summary>
        private void PollFreqUpDown_ValueChanged(object sender, EventArgs e)
        {
            gamepads.PollFrequency = (int)pollFreqBox.Value;
        }

        /// <summary>
        /// Event Handler for Serial Port selection changes.
        /// </summary>
        private void ComPortList_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedComPortLabel.Text = comPortList.SelectedItem.ToString();
        }

        /// <summary>
        /// Event Handler for Serial Baud input validation.
        /// </summary>
        private void BaudInput_Validating(object sender, CancelEventArgs e)
        {
            int numberEntered;
            if (!int.TryParse(baudInput.Text, out numberEntered))
            {
                MessageBox.Show("You need to enter an integer.", "Validation Error");
                baudInput.Text = Properties.Settings.Default.baudRate.ToString();
            }
        }

        /// <summary>
        /// Event Handler for Vibration Enabled Checkbox modifications.
        /// </summary>
        private void VibeSelectedBox_CheckedChanged(object sender, EventArgs e)
        {
            var checkBox = (CheckBox)sender;
            if (!checkBox.Checked)
            {
                vibePadTimeBox.Enabled = false;
                leftVibeFreqBox.Enabled = false;
                rightVibeFreqBox.Enabled = false;
            }
            else
            {
                vibePadTimeBox.Enabled = true;
                leftVibeFreqBox.Enabled = true;
                rightVibeFreqBox.Enabled = true;
            }
        }

        /// <summary>
        /// Event Handler for Load Default settings button clicks.
        /// </summary>
        private void LoadDefaultsBtn_Click(object sender, EventArgs e)
        {
            fillSerialOptions();
            Properties.Settings.Default.Reload();
        }

        /// <summary>
        /// Event Handler for Save Settings button clicks.
        /// </summary>
        private void SaveSettingsBtn_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.comPort = comPortList.SelectedItem.ToString();
            Properties.Settings.Default.baudRate = int.Parse(baudInput.Text);
            Properties.Settings.Default.parityIdx = parityInput.SelectedIndex;
            Properties.Settings.Default.flowControlIdx = flowControlInput.SelectedIndex;
            Properties.Settings.Default.dataBitsIdx = dataBitsInput.SelectedIndex;
            Properties.Settings.Default.stopBitsIdx = stopBitsInput.SelectedIndex;

            Properties.Settings.Default.pollingFrequency = pollFreqBox.Value;
            Properties.Settings.Default.vibeSelectedPad = vibeSelectedBox.Checked;
            Properties.Settings.Default.vibePadTime = vibePadTimeBox.Value;
            Properties.Settings.Default.leftMotorFreq = leftVibeFreqBox.Value;
            Properties.Settings.Default.rightMotorFreq = rightVibeFreqBox.Value;

            Properties.Settings.Default.Save();
        }
    }
}
