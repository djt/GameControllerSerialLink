﻿namespace ControllerSerialLink
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.padSelLabel = new System.Windows.Forms.Label();
            this.batInfoLabel = new System.Windows.Forms.Label();
            this.batTypeLabel = new System.Windows.Forms.Label();
            this.batLevelLabel = new System.Windows.Forms.Label();
            this.padCapLabel = new System.Windows.Forms.Label();
            this.serialPort = new System.IO.Ports.SerialPort(this.components);
            this.capTypeLabel = new System.Windows.Forms.Label();
            this.capFlagLabel = new System.Windows.Forms.Label();
            this.buttonListBox = new System.Windows.Forms.ListBox();
            this.padConnLabel = new System.Windows.Forms.Label();
            this.pad1ConnBox = new System.Windows.Forms.CheckBox();
            this.pad2ConnBox = new System.Windows.Forms.CheckBox();
            this.pad3ConnBox = new System.Windows.Forms.CheckBox();
            this.pad4ConnBox = new System.Windows.Forms.CheckBox();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.selectedPadLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.padConnectedLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.selectedComPortLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.comStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.pad4Sel = new System.Windows.Forms.RadioButton();
            this.pad3Sel = new System.Windows.Forms.RadioButton();
            this.pad2Sel = new System.Windows.Forms.RadioButton();
            this.pad1Sel = new System.Windows.Forms.RadioButton();
            this.padInfoPanel = new System.Windows.Forms.Panel();
            this.tabBar = new System.Windows.Forms.TabControl();
            this.settingsTab = new System.Windows.Forms.TabPage();
            this.loadDefaultsBtn = new System.Windows.Forms.Button();
            this.saveSettingsBtn = new System.Windows.Forms.Button();
            this.controllerPrefsGroup = new System.Windows.Forms.GroupBox();
            this.vibeHighFreqLabel = new System.Windows.Forms.Label();
            this.vibeLowFreqLabel = new System.Windows.Forms.Label();
            this.vibeMotorSpeedLabel = new System.Windows.Forms.Label();
            this.vibePadTimeLabel = new System.Windows.Forms.Label();
            this.vibePadTimeBox = new System.Windows.Forms.NumericUpDown();
            this.rightVibeFreqBox = new System.Windows.Forms.NumericUpDown();
            this.leftVibeFreqBox = new System.Windows.Forms.NumericUpDown();
            this.vibeSelectedBox = new System.Windows.Forms.CheckBox();
            this.pollFreqBox = new System.Windows.Forms.NumericUpDown();
            this.pollFreqLabel = new System.Windows.Forms.Label();
            this.comPortGroup = new System.Windows.Forms.GroupBox();
            this.stopBitsInput = new System.Windows.Forms.ComboBox();
            this.dataBitsInput = new System.Windows.Forms.ComboBox();
            this.serialFrameSizeLabel = new System.Windows.Forms.Label();
            this.flowControlInput = new System.Windows.Forms.ComboBox();
            this.flowControlLabel = new System.Windows.Forms.Label();
            this.parityInput = new System.Windows.Forms.ComboBox();
            this.parityLabel = new System.Windows.Forms.Label();
            this.bitsLabel = new System.Windows.Forms.Label();
            this.baudInput = new System.Windows.Forms.TextBox();
            this.stopBitsLabel = new System.Windows.Forms.Label();
            this.dataBitsLabel = new System.Windows.Forms.Label();
            this.baudRateLabel = new System.Windows.Forms.Label();
            this.comPortList = new System.Windows.Forms.ListBox();
            this.padInfoTab = new System.Windows.Forms.TabPage();
            this.eventLogTab = new System.Windows.Forms.TabPage();
            this.eventPanel = new System.Windows.Forms.Panel();
            this.eventLogBox = new System.Windows.Forms.TextBox();
            this.connectionButton = new System.Windows.Forms.Button();
            this.statusStrip.SuspendLayout();
            this.padInfoPanel.SuspendLayout();
            this.tabBar.SuspendLayout();
            this.settingsTab.SuspendLayout();
            this.controllerPrefsGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vibePadTimeBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightVibeFreqBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leftVibeFreqBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pollFreqBox)).BeginInit();
            this.comPortGroup.SuspendLayout();
            this.padInfoTab.SuspendLayout();
            this.eventLogTab.SuspendLayout();
            this.eventPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // padSelLabel
            // 
            this.padSelLabel.AutoSize = true;
            this.padSelLabel.Location = new System.Drawing.Point(27, 17);
            this.padSelLabel.Name = "padSelLabel";
            this.padSelLabel.Size = new System.Drawing.Size(89, 13);
            this.padSelLabel.TabIndex = 0;
            this.padSelLabel.Text = "Controller Details:";
            // 
            // batInfoLabel
            // 
            this.batInfoLabel.AutoSize = true;
            this.batInfoLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.batInfoLabel.Location = new System.Drawing.Point(9, 46);
            this.batInfoLabel.Name = "batInfoLabel";
            this.batInfoLabel.Size = new System.Drawing.Size(140, 45);
            this.batInfoLabel.TabIndex = 4;
            this.batInfoLabel.Text = "Battery Information\r\n       Type:\r\n      Level:";
            // 
            // batTypeLabel
            // 
            this.batTypeLabel.AutoSize = true;
            this.batTypeLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.batTypeLabel.Location = new System.Drawing.Point(102, 61);
            this.batTypeLabel.Name = "batTypeLabel";
            this.batTypeLabel.Size = new System.Drawing.Size(28, 15);
            this.batTypeLabel.TabIndex = 5;
            this.batTypeLabel.Text = "N/A";
            // 
            // batLevelLabel
            // 
            this.batLevelLabel.AutoSize = true;
            this.batLevelLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.batLevelLabel.Location = new System.Drawing.Point(102, 76);
            this.batLevelLabel.Name = "batLevelLabel";
            this.batLevelLabel.Size = new System.Drawing.Size(28, 15);
            this.batLevelLabel.TabIndex = 6;
            this.batLevelLabel.Text = "N/A";
            // 
            // padCapLabel
            // 
            this.padCapLabel.AutoSize = true;
            this.padCapLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.padCapLabel.Location = new System.Drawing.Point(9, 94);
            this.padCapLabel.Name = "padCapLabel";
            this.padCapLabel.Size = new System.Drawing.Size(168, 60);
            this.padCapLabel.TabIndex = 7;
            this.padCapLabel.Text = "Controller Capabilities\r\n       Type:\r\n      Flags:\r\n    Buttons:";
            // 
            // capTypeLabel
            // 
            this.capTypeLabel.AutoSize = true;
            this.capTypeLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.capTypeLabel.Location = new System.Drawing.Point(102, 110);
            this.capTypeLabel.Name = "capTypeLabel";
            this.capTypeLabel.Size = new System.Drawing.Size(28, 15);
            this.capTypeLabel.TabIndex = 8;
            this.capTypeLabel.Text = "N/A";
            // 
            // capFlagLabel
            // 
            this.capFlagLabel.AutoSize = true;
            this.capFlagLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.capFlagLabel.Location = new System.Drawing.Point(102, 125);
            this.capFlagLabel.Name = "capFlagLabel";
            this.capFlagLabel.Size = new System.Drawing.Size(28, 15);
            this.capFlagLabel.TabIndex = 9;
            this.capFlagLabel.Text = "N/A";
            // 
            // buttonListBox
            // 
            this.buttonListBox.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonListBox.FormattingEnabled = true;
            this.buttonListBox.ItemHeight = 15;
            this.buttonListBox.Items.AddRange(new object[] {
            "N/A"});
            this.buttonListBox.Location = new System.Drawing.Point(101, 143);
            this.buttonListBox.Name = "buttonListBox";
            this.buttonListBox.ScrollAlwaysVisible = true;
            this.buttonListBox.Size = new System.Drawing.Size(136, 109);
            this.buttonListBox.TabIndex = 10;
            // 
            // padConnLabel
            // 
            this.padConnLabel.AutoSize = true;
            this.padConnLabel.Location = new System.Drawing.Point(9, 17);
            this.padConnLabel.Name = "padConnLabel";
            this.padConnLabel.Size = new System.Drawing.Size(114, 13);
            this.padConnLabel.TabIndex = 12;
            this.padConnLabel.Text = "Connected Controllers:";
            // 
            // pad1ConnBox
            // 
            this.pad1ConnBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.pad1ConnBox.AutoCheck = false;
            this.pad1ConnBox.AutoSize = true;
            this.pad1ConnBox.Enabled = false;
            this.pad1ConnBox.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pad1ConnBox.Location = new System.Drawing.Point(129, 13);
            this.pad1ConnBox.Name = "pad1ConnBox";
            this.pad1ConnBox.Size = new System.Drawing.Size(23, 23);
            this.pad1ConnBox.TabIndex = 0;
            this.pad1ConnBox.Text = "1";
            this.pad1ConnBox.UseVisualStyleBackColor = true;
            // 
            // pad2ConnBox
            // 
            this.pad2ConnBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.pad2ConnBox.AutoCheck = false;
            this.pad2ConnBox.AutoSize = true;
            this.pad2ConnBox.Enabled = false;
            this.pad2ConnBox.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pad2ConnBox.Location = new System.Drawing.Point(159, 12);
            this.pad2ConnBox.Name = "pad2ConnBox";
            this.pad2ConnBox.Size = new System.Drawing.Size(23, 23);
            this.pad2ConnBox.TabIndex = 13;
            this.pad2ConnBox.Text = "2";
            this.pad2ConnBox.UseVisualStyleBackColor = true;
            // 
            // pad3ConnBox
            // 
            this.pad3ConnBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.pad3ConnBox.AutoCheck = false;
            this.pad3ConnBox.AutoSize = true;
            this.pad3ConnBox.Enabled = false;
            this.pad3ConnBox.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pad3ConnBox.Location = new System.Drawing.Point(188, 13);
            this.pad3ConnBox.Name = "pad3ConnBox";
            this.pad3ConnBox.Size = new System.Drawing.Size(23, 23);
            this.pad3ConnBox.TabIndex = 14;
            this.pad3ConnBox.Text = "3";
            this.pad3ConnBox.UseVisualStyleBackColor = true;
            // 
            // pad4ConnBox
            // 
            this.pad4ConnBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.pad4ConnBox.AutoCheck = false;
            this.pad4ConnBox.AutoSize = true;
            this.pad4ConnBox.Enabled = false;
            this.pad4ConnBox.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pad4ConnBox.Location = new System.Drawing.Point(217, 13);
            this.pad4ConnBox.Name = "pad4ConnBox";
            this.pad4ConnBox.Size = new System.Drawing.Size(23, 23);
            this.pad4ConnBox.TabIndex = 15;
            this.pad4ConnBox.Text = "4";
            this.pad4ConnBox.UseVisualStyleBackColor = true;
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectedPadLabel,
            this.padConnectedLabel,
            this.selectedComPortLabel,
            this.comStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 357);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(381, 24);
            this.statusStrip.TabIndex = 16;
            this.statusStrip.Text = "statusStrip1";
            // 
            // selectedPadLabel
            // 
            this.selectedPadLabel.Name = "selectedPadLabel";
            this.selectedPadLabel.Size = new System.Drawing.Size(68, 19);
            this.selectedPadLabel.Text = "Controller ?";
            // 
            // padConnectedLabel
            // 
            this.padConnectedLabel.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.padConnectedLabel.Name = "padConnectedLabel";
            this.padConnectedLabel.Size = new System.Drawing.Size(82, 19);
            this.padConnectedLabel.Text = "Connecting...";
            // 
            // selectedComPortLabel
            // 
            this.selectedComPortLabel.Name = "selectedComPortLabel";
            this.selectedComPortLabel.Size = new System.Drawing.Size(40, 19);
            this.selectedComPortLabel.Text = "COM?";
            // 
            // comStatusLabel
            // 
            this.comStatusLabel.Name = "comStatusLabel";
            this.comStatusLabel.Size = new System.Drawing.Size(78, 19);
            this.comStatusLabel.Text = "Connecting...";
            // 
            // pad4Sel
            // 
            this.pad4Sel.AutoSize = true;
            this.pad4Sel.Location = new System.Drawing.Point(235, 15);
            this.pad4Sel.Name = "pad4Sel";
            this.pad4Sel.Size = new System.Drawing.Size(31, 17);
            this.pad4Sel.TabIndex = 19;
            this.pad4Sel.TabStop = true;
            this.pad4Sel.Tag = 3;
            this.pad4Sel.Text = "4";
            this.pad4Sel.UseVisualStyleBackColor = true;
            // 
            // pad3Sel
            // 
            this.pad3Sel.AutoSize = true;
            this.pad3Sel.Location = new System.Drawing.Point(206, 15);
            this.pad3Sel.Name = "pad3Sel";
            this.pad3Sel.Size = new System.Drawing.Size(31, 17);
            this.pad3Sel.TabIndex = 18;
            this.pad3Sel.TabStop = true;
            this.pad3Sel.Tag = 2;
            this.pad3Sel.Text = "3";
            this.pad3Sel.UseVisualStyleBackColor = true;
            // 
            // pad2Sel
            // 
            this.pad2Sel.AutoSize = true;
            this.pad2Sel.Location = new System.Drawing.Point(177, 15);
            this.pad2Sel.Name = "pad2Sel";
            this.pad2Sel.Size = new System.Drawing.Size(31, 17);
            this.pad2Sel.TabIndex = 17;
            this.pad2Sel.TabStop = true;
            this.pad2Sel.Tag = 1;
            this.pad2Sel.Text = "2";
            this.pad2Sel.UseVisualStyleBackColor = true;
            // 
            // pad1Sel
            // 
            this.pad1Sel.AutoSize = true;
            this.pad1Sel.Checked = true;
            this.pad1Sel.Location = new System.Drawing.Point(144, 15);
            this.pad1Sel.Name = "pad1Sel";
            this.pad1Sel.Size = new System.Drawing.Size(31, 17);
            this.pad1Sel.TabIndex = 16;
            this.pad1Sel.TabStop = true;
            this.pad1Sel.Tag = 0;
            this.pad1Sel.Text = "1";
            this.pad1Sel.UseVisualStyleBackColor = true;
            // 
            // padInfoPanel
            // 
            this.padInfoPanel.Controls.Add(this.pad4Sel);
            this.padInfoPanel.Controls.Add(this.buttonListBox);
            this.padInfoPanel.Controls.Add(this.pad3Sel);
            this.padInfoPanel.Controls.Add(this.capFlagLabel);
            this.padInfoPanel.Controls.Add(this.pad2Sel);
            this.padInfoPanel.Controls.Add(this.capTypeLabel);
            this.padInfoPanel.Controls.Add(this.pad1Sel);
            this.padInfoPanel.Controls.Add(this.padCapLabel);
            this.padInfoPanel.Controls.Add(this.padSelLabel);
            this.padInfoPanel.Controls.Add(this.batLevelLabel);
            this.padInfoPanel.Controls.Add(this.batTypeLabel);
            this.padInfoPanel.Controls.Add(this.batInfoLabel);
            this.padInfoPanel.Location = new System.Drawing.Point(6, 6);
            this.padInfoPanel.Name = "padInfoPanel";
            this.padInfoPanel.Size = new System.Drawing.Size(337, 279);
            this.padInfoPanel.TabIndex = 18;
            // 
            // tabBar
            // 
            this.tabBar.Controls.Add(this.settingsTab);
            this.tabBar.Controls.Add(this.padInfoTab);
            this.tabBar.Controls.Add(this.eventLogTab);
            this.tabBar.Location = new System.Drawing.Point(12, 42);
            this.tabBar.Name = "tabBar";
            this.tabBar.SelectedIndex = 0;
            this.tabBar.Size = new System.Drawing.Size(357, 312);
            this.tabBar.TabIndex = 19;
            // 
            // settingsTab
            // 
            this.settingsTab.Controls.Add(this.loadDefaultsBtn);
            this.settingsTab.Controls.Add(this.saveSettingsBtn);
            this.settingsTab.Controls.Add(this.controllerPrefsGroup);
            this.settingsTab.Controls.Add(this.comPortGroup);
            this.settingsTab.Location = new System.Drawing.Point(4, 22);
            this.settingsTab.Name = "settingsTab";
            this.settingsTab.Padding = new System.Windows.Forms.Padding(3);
            this.settingsTab.Size = new System.Drawing.Size(349, 286);
            this.settingsTab.TabIndex = 0;
            this.settingsTab.Text = "Settings";
            this.settingsTab.UseVisualStyleBackColor = true;
            // 
            // loadDefaultsBtn
            // 
            this.loadDefaultsBtn.Location = new System.Drawing.Point(215, 257);
            this.loadDefaultsBtn.Name = "loadDefaultsBtn";
            this.loadDefaultsBtn.Size = new System.Drawing.Size(82, 23);
            this.loadDefaultsBtn.TabIndex = 5;
            this.loadDefaultsBtn.Text = "Load Defaults";
            this.loadDefaultsBtn.UseVisualStyleBackColor = true;
            this.loadDefaultsBtn.Click += new System.EventHandler(this.LoadDefaultsBtn_Click);
            // 
            // saveSettingsBtn
            // 
            this.saveSettingsBtn.Location = new System.Drawing.Point(106, 257);
            this.saveSettingsBtn.Name = "saveSettingsBtn";
            this.saveSettingsBtn.Size = new System.Drawing.Size(84, 23);
            this.saveSettingsBtn.TabIndex = 4;
            this.saveSettingsBtn.Text = "Save Settings";
            this.saveSettingsBtn.UseVisualStyleBackColor = true;
            this.saveSettingsBtn.Click += new System.EventHandler(this.SaveSettingsBtn_Click);
            // 
            // controllerPrefsGroup
            // 
            this.controllerPrefsGroup.Controls.Add(this.vibeHighFreqLabel);
            this.controllerPrefsGroup.Controls.Add(this.vibeLowFreqLabel);
            this.controllerPrefsGroup.Controls.Add(this.vibeMotorSpeedLabel);
            this.controllerPrefsGroup.Controls.Add(this.vibePadTimeLabel);
            this.controllerPrefsGroup.Controls.Add(this.vibePadTimeBox);
            this.controllerPrefsGroup.Controls.Add(this.rightVibeFreqBox);
            this.controllerPrefsGroup.Controls.Add(this.leftVibeFreqBox);
            this.controllerPrefsGroup.Controls.Add(this.vibeSelectedBox);
            this.controllerPrefsGroup.Controls.Add(this.pollFreqBox);
            this.controllerPrefsGroup.Controls.Add(this.pollFreqLabel);
            this.controllerPrefsGroup.Location = new System.Drawing.Point(6, 110);
            this.controllerPrefsGroup.Name = "controllerPrefsGroup";
            this.controllerPrefsGroup.Size = new System.Drawing.Size(337, 141);
            this.controllerPrefsGroup.TabIndex = 3;
            this.controllerPrefsGroup.TabStop = false;
            this.controllerPrefsGroup.Text = "Game Controller Preferences";
            // 
            // vibeHighFreqLabel
            // 
            this.vibeHighFreqLabel.AutoSize = true;
            this.vibeHighFreqLabel.Location = new System.Drawing.Point(211, 90);
            this.vibeHighFreqLabel.Name = "vibeHighFreqLabel";
            this.vibeHighFreqLabel.Size = new System.Drawing.Size(73, 13);
            this.vibeHighFreqLabel.TabIndex = 9;
            this.vibeHighFreqLabel.Text = "High Freq. (R)";
            // 
            // vibeLowFreqLabel
            // 
            this.vibeLowFreqLabel.AutoSize = true;
            this.vibeLowFreqLabel.Location = new System.Drawing.Point(133, 90);
            this.vibeLowFreqLabel.Name = "vibeLowFreqLabel";
            this.vibeLowFreqLabel.Size = new System.Drawing.Size(69, 13);
            this.vibeLowFreqLabel.TabIndex = 8;
            this.vibeLowFreqLabel.Text = "Low Freq. (L)";
            // 
            // vibeMotorSpeedLabel
            // 
            this.vibeMotorSpeedLabel.AutoSize = true;
            this.vibeMotorSpeedLabel.Location = new System.Drawing.Point(8, 108);
            this.vibeMotorSpeedLabel.Name = "vibeMotorSpeedLabel";
            this.vibeMotorSpeedLabel.Size = new System.Drawing.Size(117, 13);
            this.vibeMotorSpeedLabel.TabIndex = 7;
            this.vibeMotorSpeedLabel.Text = "Vibe Motor Speeds (%):";
            // 
            // vibePadTimeLabel
            // 
            this.vibePadTimeLabel.AutoSize = true;
            this.vibePadTimeLabel.Location = new System.Drawing.Point(7, 67);
            this.vibePadTimeLabel.Name = "vibePadTimeLabel";
            this.vibePadTimeLabel.Size = new System.Drawing.Size(140, 13);
            this.vibePadTimeLabel.TabIndex = 6;
            this.vibePadTimeLabel.Text = "Vibrate Gamepad Time (ms):";
            // 
            // vibePadTimeBox
            // 
            this.vibePadTimeBox.DataBindings.Add(new System.Windows.Forms.Binding("Value", global::ControllerSerialLink.Properties.Settings.Default, "vibePadTime", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.vibePadTimeBox.Increment = new decimal(new int[] {
            250,
            0,
            0,
            0});
            this.vibePadTimeBox.Location = new System.Drawing.Point(153, 65);
            this.vibePadTimeBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.vibePadTimeBox.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.vibePadTimeBox.Name = "vibePadTimeBox";
            this.vibePadTimeBox.Size = new System.Drawing.Size(56, 20);
            this.vibePadTimeBox.TabIndex = 5;
            this.vibePadTimeBox.Value = global::ControllerSerialLink.Properties.Settings.Default.vibePadTime;
            // 
            // rightVibeFreqBox
            // 
            this.rightVibeFreqBox.DataBindings.Add(new System.Windows.Forms.Binding("Value", global::ControllerSerialLink.Properties.Settings.Default, "rightMotorFreq", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.rightVibeFreqBox.Location = new System.Drawing.Point(214, 106);
            this.rightVibeFreqBox.Name = "rightVibeFreqBox";
            this.rightVibeFreqBox.Size = new System.Drawing.Size(71, 20);
            this.rightVibeFreqBox.TabIndex = 4;
            this.rightVibeFreqBox.Value = global::ControllerSerialLink.Properties.Settings.Default.rightMotorFreq;
            // 
            // leftVibeFreqBox
            // 
            this.leftVibeFreqBox.DataBindings.Add(new System.Windows.Forms.Binding("Value", global::ControllerSerialLink.Properties.Settings.Default, "leftMotorFreq", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.leftVibeFreqBox.Location = new System.Drawing.Point(136, 106);
            this.leftVibeFreqBox.Name = "leftVibeFreqBox";
            this.leftVibeFreqBox.Size = new System.Drawing.Size(72, 20);
            this.leftVibeFreqBox.TabIndex = 3;
            this.leftVibeFreqBox.Value = global::ControllerSerialLink.Properties.Settings.Default.leftMotorFreq;
            // 
            // vibeSelectedBox
            // 
            this.vibeSelectedBox.AutoSize = true;
            this.vibeSelectedBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.vibeSelectedBox.Checked = global::ControllerSerialLink.Properties.Settings.Default.vibeSelectedPad;
            this.vibeSelectedBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.vibeSelectedBox.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::ControllerSerialLink.Properties.Settings.Default, "vibeSelectedPad", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.vibeSelectedBox.Location = new System.Drawing.Point(6, 44);
            this.vibeSelectedBox.Name = "vibeSelectedBox";
            this.vibeSelectedBox.Size = new System.Drawing.Size(157, 17);
            this.vibeSelectedBox.TabIndex = 2;
            this.vibeSelectedBox.Text = "Vibrate Selected Controller?";
            this.vibeSelectedBox.UseVisualStyleBackColor = true;
            this.vibeSelectedBox.CheckedChanged += new System.EventHandler(this.VibeSelectedBox_CheckedChanged);
            // 
            // pollFreqBox
            // 
            this.pollFreqBox.DataBindings.Add(new System.Windows.Forms.Binding("Value", global::ControllerSerialLink.Properties.Settings.Default, "pollingFrequency", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.pollFreqBox.Location = new System.Drawing.Point(153, 20);
            this.pollFreqBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.pollFreqBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.pollFreqBox.Name = "pollFreqBox";
            this.pollFreqBox.Size = new System.Drawing.Size(56, 20);
            this.pollFreqBox.TabIndex = 1;
            this.pollFreqBox.Value = global::ControllerSerialLink.Properties.Settings.Default.pollingFrequency;
            this.pollFreqBox.ValueChanged += new System.EventHandler(this.PollFreqUpDown_ValueChanged);
            // 
            // pollFreqLabel
            // 
            this.pollFreqLabel.AutoSize = true;
            this.pollFreqLabel.Location = new System.Drawing.Point(7, 20);
            this.pollFreqLabel.Name = "pollFreqLabel";
            this.pollFreqLabel.Size = new System.Drawing.Size(116, 13);
            this.pollFreqLabel.TabIndex = 0;
            this.pollFreqLabel.Text = "Polling Frequency (ms):";
            // 
            // comPortGroup
            // 
            this.comPortGroup.Controls.Add(this.stopBitsInput);
            this.comPortGroup.Controls.Add(this.dataBitsInput);
            this.comPortGroup.Controls.Add(this.serialFrameSizeLabel);
            this.comPortGroup.Controls.Add(this.flowControlInput);
            this.comPortGroup.Controls.Add(this.flowControlLabel);
            this.comPortGroup.Controls.Add(this.parityInput);
            this.comPortGroup.Controls.Add(this.parityLabel);
            this.comPortGroup.Controls.Add(this.bitsLabel);
            this.comPortGroup.Controls.Add(this.baudInput);
            this.comPortGroup.Controls.Add(this.stopBitsLabel);
            this.comPortGroup.Controls.Add(this.dataBitsLabel);
            this.comPortGroup.Controls.Add(this.baudRateLabel);
            this.comPortGroup.Controls.Add(this.comPortList);
            this.comPortGroup.Location = new System.Drawing.Point(6, 6);
            this.comPortGroup.Name = "comPortGroup";
            this.comPortGroup.Size = new System.Drawing.Size(337, 98);
            this.comPortGroup.TabIndex = 2;
            this.comPortGroup.TabStop = false;
            this.comPortGroup.Text = "Serial Port Configuration";
            // 
            // stopBitsInput
            // 
            this.stopBitsInput.DataBindings.Add(new System.Windows.Forms.Binding("SelectedIndex", global::ControllerSerialLink.Properties.Settings.Default, "stopBitsIdx", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.stopBitsInput.FormattingEnabled = true;
            this.stopBitsInput.Location = new System.Drawing.Point(281, 68);
            this.stopBitsInput.Name = "stopBitsInput";
            this.stopBitsInput.Size = new System.Drawing.Size(46, 21);
            this.stopBitsInput.TabIndex = 16;
            // 
            // dataBitsInput
            // 
            this.dataBitsInput.DataBindings.Add(new System.Windows.Forms.Binding("SelectedIndex", global::ControllerSerialLink.Properties.Settings.Default, "dataBitsIdx", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.dataBitsInput.FormattingEnabled = true;
            this.dataBitsInput.Location = new System.Drawing.Point(281, 42);
            this.dataBitsInput.Name = "dataBitsInput";
            this.dataBitsInput.Size = new System.Drawing.Size(46, 21);
            this.dataBitsInput.TabIndex = 15;
            // 
            // serialFrameSizeLabel
            // 
            this.serialFrameSizeLabel.AutoSize = true;
            this.serialFrameSizeLabel.Location = new System.Drawing.Point(255, 15);
            this.serialFrameSizeLabel.Name = "serialFrameSizeLabel";
            this.serialFrameSizeLabel.Size = new System.Drawing.Size(59, 13);
            this.serialFrameSizeLabel.TabIndex = 14;
            this.serialFrameSizeLabel.Text = "Frame Size";
            // 
            // flowControlInput
            // 
            this.flowControlInput.DataBindings.Add(new System.Windows.Forms.Binding("SelectedIndex", global::ControllerSerialLink.Properties.Settings.Default, "flowControlIdx", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.flowControlInput.FormattingEnabled = true;
            this.flowControlInput.Location = new System.Drawing.Point(159, 69);
            this.flowControlInput.Name = "flowControlInput";
            this.flowControlInput.Size = new System.Drawing.Size(78, 21);
            this.flowControlInput.TabIndex = 13;
            // 
            // flowControlLabel
            // 
            this.flowControlLabel.AutoSize = true;
            this.flowControlLabel.Location = new System.Drawing.Point(86, 72);
            this.flowControlLabel.Name = "flowControlLabel";
            this.flowControlLabel.Size = new System.Drawing.Size(67, 13);
            this.flowControlLabel.TabIndex = 7;
            this.flowControlLabel.Text = "Flow control:";
            // 
            // parityInput
            // 
            this.parityInput.DataBindings.Add(new System.Windows.Forms.Binding("SelectedIndex", global::ControllerSerialLink.Properties.Settings.Default, "parityIdx", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.parityInput.FormattingEnabled = true;
            this.parityInput.Location = new System.Drawing.Point(159, 42);
            this.parityInput.Name = "parityInput";
            this.parityInput.Size = new System.Drawing.Size(77, 21);
            this.parityInput.TabIndex = 12;
            // 
            // parityLabel
            // 
            this.parityLabel.AutoSize = true;
            this.parityLabel.Location = new System.Drawing.Point(117, 45);
            this.parityLabel.Name = "parityLabel";
            this.parityLabel.Size = new System.Drawing.Size(36, 13);
            this.parityLabel.TabIndex = 6;
            this.parityLabel.Text = "Parity:";
            // 
            // bitsLabel
            // 
            this.bitsLabel.AutoSize = true;
            this.bitsLabel.Location = new System.Drawing.Point(291, 28);
            this.bitsLabel.Name = "bitsLabel";
            this.bitsLabel.Size = new System.Drawing.Size(23, 13);
            this.bitsLabel.TabIndex = 11;
            this.bitsLabel.Text = "bits";
            // 
            // baudInput
            // 
            this.baudInput.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::ControllerSerialLink.Properties.Settings.Default, "baudRate", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.baudInput.Location = new System.Drawing.Point(159, 16);
            this.baudInput.Name = "baudInput";
            this.baudInput.Size = new System.Drawing.Size(78, 20);
            this.baudInput.TabIndex = 8;
            this.baudInput.Validating += new System.ComponentModel.CancelEventHandler(this.BaudInput_Validating);
            // 
            // stopBitsLabel
            // 
            this.stopBitsLabel.AutoSize = true;
            this.stopBitsLabel.Location = new System.Drawing.Point(243, 71);
            this.stopBitsLabel.Name = "stopBitsLabel";
            this.stopBitsLabel.Size = new System.Drawing.Size(32, 13);
            this.stopBitsLabel.TabIndex = 5;
            this.stopBitsLabel.Text = "Stop:";
            // 
            // dataBitsLabel
            // 
            this.dataBitsLabel.AutoSize = true;
            this.dataBitsLabel.Location = new System.Drawing.Point(242, 45);
            this.dataBitsLabel.Name = "dataBitsLabel";
            this.dataBitsLabel.Size = new System.Drawing.Size(33, 13);
            this.dataBitsLabel.TabIndex = 4;
            this.dataBitsLabel.Text = "Data:";
            // 
            // baudRateLabel
            // 
            this.baudRateLabel.AutoSize = true;
            this.baudRateLabel.Location = new System.Drawing.Point(97, 19);
            this.baudRateLabel.Name = "baudRateLabel";
            this.baudRateLabel.Size = new System.Drawing.Size(56, 13);
            this.baudRateLabel.TabIndex = 3;
            this.baudRateLabel.Text = "Baud rate:";
            // 
            // comPortList
            // 
            this.comPortList.FormattingEnabled = true;
            this.comPortList.Location = new System.Drawing.Point(6, 19);
            this.comPortList.Name = "comPortList";
            this.comPortList.Size = new System.Drawing.Size(74, 69);
            this.comPortList.TabIndex = 17;
            this.comPortList.SelectedIndexChanged += new System.EventHandler(this.ComPortList_SelectedIndexChanged);
            // 
            // padInfoTab
            // 
            this.padInfoTab.Controls.Add(this.padInfoPanel);
            this.padInfoTab.Location = new System.Drawing.Point(4, 22);
            this.padInfoTab.Name = "padInfoTab";
            this.padInfoTab.Padding = new System.Windows.Forms.Padding(3);
            this.padInfoTab.Size = new System.Drawing.Size(349, 286);
            this.padInfoTab.TabIndex = 1;
            this.padInfoTab.Text = "Controller Info";
            this.padInfoTab.UseVisualStyleBackColor = true;
            // 
            // eventLogTab
            // 
            this.eventLogTab.Controls.Add(this.eventPanel);
            this.eventLogTab.Location = new System.Drawing.Point(4, 22);
            this.eventLogTab.Name = "eventLogTab";
            this.eventLogTab.Padding = new System.Windows.Forms.Padding(3);
            this.eventLogTab.Size = new System.Drawing.Size(349, 286);
            this.eventLogTab.TabIndex = 2;
            this.eventLogTab.Text = "Event Log";
            this.eventLogTab.UseVisualStyleBackColor = true;
            // 
            // eventPanel
            // 
            this.eventPanel.Controls.Add(this.eventLogBox);
            this.eventPanel.Location = new System.Drawing.Point(6, 6);
            this.eventPanel.Name = "eventPanel";
            this.eventPanel.Size = new System.Drawing.Size(337, 279);
            this.eventPanel.TabIndex = 0;
            // 
            // eventLogBox
            // 
            this.eventLogBox.CausesValidation = false;
            this.eventLogBox.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eventLogBox.Location = new System.Drawing.Point(3, 3);
            this.eventLogBox.Multiline = true;
            this.eventLogBox.Name = "eventLogBox";
            this.eventLogBox.ReadOnly = true;
            this.eventLogBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.eventLogBox.Size = new System.Drawing.Size(331, 273);
            this.eventLogBox.TabIndex = 0;
            // 
            // connectionButton
            // 
            this.connectionButton.Location = new System.Drawing.Point(259, 12);
            this.connectionButton.Name = "connectionButton";
            this.connectionButton.Size = new System.Drawing.Size(106, 23);
            this.connectionButton.TabIndex = 20;
            this.connectionButton.Text = "Connect Serial";
            this.connectionButton.UseVisualStyleBackColor = true;
            this.connectionButton.Click += new System.EventHandler(this.ConnectionButton_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(381, 381);
            this.Controls.Add(this.connectionButton);
            this.Controls.Add(this.tabBar);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.padConnLabel);
            this.Controls.Add(this.pad4ConnBox);
            this.Controls.Add(this.pad1ConnBox);
            this.Controls.Add(this.pad3ConnBox);
            this.Controls.Add(this.pad2ConnBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MainWindow";
            this.Text = "Controller Serial Link";
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.padInfoPanel.ResumeLayout(false);
            this.padInfoPanel.PerformLayout();
            this.tabBar.ResumeLayout(false);
            this.settingsTab.ResumeLayout(false);
            this.controllerPrefsGroup.ResumeLayout(false);
            this.controllerPrefsGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vibePadTimeBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightVibeFreqBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leftVibeFreqBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pollFreqBox)).EndInit();
            this.comPortGroup.ResumeLayout(false);
            this.comPortGroup.PerformLayout();
            this.padInfoTab.ResumeLayout(false);
            this.eventLogTab.ResumeLayout(false);
            this.eventPanel.ResumeLayout(false);
            this.eventPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label padSelLabel;
        private System.Windows.Forms.Label batInfoLabel;
        private System.Windows.Forms.Label batTypeLabel;
        private System.Windows.Forms.Label batLevelLabel;
        private System.Windows.Forms.Label padCapLabel;
        private System.IO.Ports.SerialPort serialPort;
        private System.Windows.Forms.Label capTypeLabel;
        private System.Windows.Forms.Label capFlagLabel;
        private System.Windows.Forms.ListBox buttonListBox;
        private System.Windows.Forms.Label padConnLabel;
        private System.Windows.Forms.CheckBox pad1ConnBox;
        private System.Windows.Forms.CheckBox pad2ConnBox;
        private System.Windows.Forms.CheckBox pad3ConnBox;
        private System.Windows.Forms.CheckBox pad4ConnBox;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel selectedPadLabel;
        private System.Windows.Forms.ToolStripStatusLabel padConnectedLabel;
        private System.Windows.Forms.RadioButton pad4Sel;
        private System.Windows.Forms.RadioButton pad3Sel;
        private System.Windows.Forms.RadioButton pad2Sel;
        private System.Windows.Forms.RadioButton pad1Sel;
        private System.Windows.Forms.Panel padInfoPanel;
        private System.Windows.Forms.TabControl tabBar;
        private System.Windows.Forms.TabPage padInfoTab;
        private System.Windows.Forms.TabPage eventLogTab;
        private System.Windows.Forms.TabPage settingsTab;
        private System.Windows.Forms.GroupBox comPortGroup;
        private System.Windows.Forms.TextBox baudInput;
        private System.Windows.Forms.Label flowControlLabel;
        private System.Windows.Forms.Label parityLabel;
        private System.Windows.Forms.Label stopBitsLabel;
        private System.Windows.Forms.Label dataBitsLabel;
        private System.Windows.Forms.Label baudRateLabel;
        private System.Windows.Forms.Label serialFrameSizeLabel;
        private System.Windows.Forms.ComboBox flowControlInput;
        private System.Windows.Forms.ComboBox parityInput;
        private System.Windows.Forms.Label bitsLabel;
        private System.Windows.Forms.ComboBox stopBitsInput;
        private System.Windows.Forms.ComboBox dataBitsInput;
        private System.Windows.Forms.ListBox comPortList;
        private System.Windows.Forms.GroupBox controllerPrefsGroup;
        private System.Windows.Forms.NumericUpDown pollFreqBox;
        private System.Windows.Forms.Label pollFreqLabel;
        private System.Windows.Forms.CheckBox vibeSelectedBox;
        private System.Windows.Forms.Label vibePadTimeLabel;
        private System.Windows.Forms.NumericUpDown vibePadTimeBox;
        private System.Windows.Forms.NumericUpDown rightVibeFreqBox;
        private System.Windows.Forms.NumericUpDown leftVibeFreqBox;
        private System.Windows.Forms.Label vibeMotorSpeedLabel;
        private System.Windows.Forms.Label vibeHighFreqLabel;
        private System.Windows.Forms.Label vibeLowFreqLabel;
        private System.Windows.Forms.Button saveSettingsBtn;
        private System.Windows.Forms.Button loadDefaultsBtn;
        private System.Windows.Forms.ToolStripStatusLabel selectedComPortLabel;
        private System.Windows.Forms.ToolStripStatusLabel comStatusLabel;
        private System.Windows.Forms.Panel eventPanel;
        private System.Windows.Forms.TextBox eventLogBox;
        private System.Windows.Forms.Button connectionButton;
    }
}

