﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Threading.Tasks;
using System.IO.Ports;

namespace ControllerSerialLink
{
    public class ParitySource : Component, IListSource
    {
        public struct Field
        {
            public string name { get; set; }
            public System.IO.Ports.Parity value { get; set; }
            public Field(string argName, Parity argValue)
            {
                name = argName;
                value = argValue;
            }
        }

        public ParitySource() { }

        public ParitySource(IContainer container)
        {
            container.Add(this);
        }

        #region IListSource Members
        bool IListSource.ContainsListCollection
        {
            get { return false; }
        }

        System.Collections.IList IListSource.GetList()
        {
            BindingList<Field> values = new BindingList<Field>();

            values.Add(new Field("None", Parity.None));
            values.Add(new Field("Odd", Parity.Odd));
            values.Add(new Field("Even", Parity.Even));
            values.Add(new Field("Mark", Parity.Mark));
            values.Add(new Field("Space", Parity.Space));

            return values;
        }
        #endregion
    }

    public class FlowControlSource : Component, IListSource
    {
        public struct Field
        {
            public string name { get; set; }
            public System.IO.Ports.Handshake value { get; set; }
            public Field(string argName, Handshake argValue)
            {
                name = argName;
                value = argValue;
            }
        }

        public FlowControlSource() { }

        public FlowControlSource(IContainer container)
        {
            container.Add(this);
        }

        #region IListSource Members
        bool IListSource.ContainsListCollection
        {
            get { return false; }
        }
        System.Collections.IList IListSource.GetList()
        {
            BindingList<Field> values = new BindingList<Field>();

            values.Add(new Field("None",     Handshake.None));
            values.Add(new Field("XON/XOFF", Handshake.XOnXOff));
            values.Add(new Field("RTS/CTS",  Handshake.RequestToSend));
            values.Add(new Field("DSR/DTR",  Handshake.RequestToSendXOnXOff));

            return values;
        }
        #endregion
    }
}
