﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Timers;
using SharpDX.XInput;

namespace ControllerSerialLink
{
    class GameControllers
    {
        public struct GameController
        {
            public UserIndex controllerIdx;
            public Controller controller;
            public State lastState;
        }
        private GameController[] gameControllers = new GameController[4];

        private Timer pollFreqTimer = new Timer();
        private Timer vibeStopTimer = new Timer();
        private bool pollLock = false;

        private List<int> lastConnectedPads = new List<int>();

        /// <summary>
        /// Interval in milliseconds at which to poll the device.
        /// </summary>
        public double PollFrequency
        {
            get { return pollFreqTimer.Interval; }
            set { pollFreqTimer.Interval = value; }
        }

        /// <summary>
        /// This event is raised when a Game Controller is connected or disconnected.
        /// </summary>
        public event EventHandler<ConnectionEventArgs> ConnectionChanged;

        /// <summary>
        /// Arguments for Game Controller input events. Contains the Controller Pad State.
        /// </summary>
        public class ConnectionEventArgs : EventArgs
        {
            public bool Enabled { get; }
            public ConnectionEventArgs(bool padEnabled)
            {
                Enabled = padEnabled;
            }
        }

        /// <summary>
        /// This event is raised when a Game Controller's input is changed.
        /// </summary>
        public event EventHandler<InputEventArgs> InputChanged;

        /// <summary>
        /// Arguments for Game Controller input events. Contains the Controller Pad State.
        /// </summary>
        public class InputEventArgs : EventArgs
        {
            public State State { get; }
            public InputEventArgs(State padState)
            {
                State = padState;
            }
        }

        /// <summary>
        /// Class containing all of the Game Controller and actions to perform on them.
        /// </summary>
        /// <param name="pollFreq">Interval in milliseconds to poll the controllers at.</param>
        public GameControllers(double pollFreq)
        {
            PollFrequency = pollFreq;
            for (UserIndex i = UserIndex.One; i <= UserIndex.Four; i++)
            {
                int padIdx = (int)i;
                gameControllers[padIdx].controllerIdx = i;
                gameControllers[padIdx].controller = new Controller(i);
            }
            pollFreqTimer.AutoReset = true;
            pollFreqTimer.Elapsed += new ElapsedEventHandler(pollControllers);
        }

        /// <summary>
        /// Enables or disables polling of Game Controllers.
        /// </summary>
        /// <param name="pollEnable">Boolean</param>
        public void EnablePolling(bool pollEnable)
        {
            pollFreqTimer.Enabled = pollEnable;
        }

        /// <summary>
        /// Timer event handler for the Poll Frequency Timer.
        /// This raises events to subscribers on Game Controller connection, disconnection, and input events.
        /// </summary>
        /// <param name="sender">The Poll Frequency Timer</param>
        /// <param name="e">Elapsed Event Arguments</param>
        private void pollControllers(object sender, EventArgs e)
        {
            if (!pollLock)
            {
                pollLock = true;
                var connectedPads = GetConnectedPads();
                if (connectedPads.Count != lastConnectedPads.Count)
                {
                    var removedPads = lastConnectedPads.Except(connectedPads);
                    var addedPads = connectedPads.Except(lastConnectedPads);
                    foreach (int i in addedPads)
                    {
                        gameControllers[i].lastState = GetPadController(i).GetState();
                        if (ConnectionChanged != null)
                            ConnectionChanged(gameControllers[i], new ConnectionEventArgs(true));
                    }
                    foreach (int i in removedPads)
                    {
                        gameControllers[i].lastState = new State();
                        var disconnectedPad = new List<int>();
                        disconnectedPad.Add(i);
                        connectedPads = connectedPads.Except(disconnectedPad).ToList();
                        if (ConnectionChanged != null)
                            ConnectionChanged(gameControllers[i], new ConnectionEventArgs(false));
                    }
                }
                if (connectedPads.Count > 0)
                {
                    foreach (int pad in connectedPads)
                    {
                        ref var lastState = ref gameControllers[pad].lastState;
                        var padState = GetPadController(pad).GetState();
                        if (padState.PacketNumber != lastState.PacketNumber)
                        {
                            if (InputChanged != null)
                            {
                                InputChanged(gameControllers[pad], new InputEventArgs(padState));
                            }
                            lastState = padState;
                        }
                    }
                }
                lastConnectedPads = connectedPads;
                pollLock = false;
            }
        }

        /// <summary>
        /// Check the status of all currently connected controllers.
        /// </summary>
        /// <returns>Return a List of connected Game Controller indexes.</returns>
        private List<int> GetConnectedPads()
        {
            List<int> pads = new List<int>();
            foreach (GameController pad in gameControllers)
            {
                if (pad.controller.IsConnected) pads.Add((int)pad.controllerIdx);
            }
            return pads;
        }

        /// <summary>
        /// Get the Controller object associated with a Game Controller. Index is between 0 and 3.
        /// </summary>
        /// <param name="index">Game Controller index</param>
        /// <returns>A SharpDX.XInput.Controller or null if index is invalid.</returns>
        public Controller GetPadController(int index)
        {
            if (index > 3 || index < 0) return null;
            return gameControllers[index].controller;
        }

        /// <summary>
        /// Vibrate the specified Game Controller for a duration specified in milliseconds.
        /// leftSpeed and rightSpeed should be between 0-65535.
        /// </summary>
        /// <param name="pad">The Game Controller to vibrate/</param>
        /// <param name="duration">Duration in miliseconds to vibrate for.</param>
        /// <param name="leftSpeed">Speed of the left (low frequency) motor.</param>
        /// <param name="rightSpeed">Speed of the right (high frequency) motor.</param>
        public void VibratePad(Controller pad, int duration, ushort leftSpeed, ushort rightSpeed)
        {
            Vibration vib = new Vibration();
            vib.LeftMotorSpeed = leftSpeed;
            vib.RightMotorSpeed = rightSpeed;
            pad.SetVibration(vib);
            vibeStopTimer.Interval = duration;
            vibeStopTimer.Elapsed += (sender, e) => haltVibeCB(sender, e, pad);
            vibeStopTimer.Start();
        }

        /// <summary>
        /// Timer event callback to stop the Game Controller vibration.
        /// </summary>
        /// <param name="timer">Timer object from the callback.</param>
        /// <param name="e">Event Arguments from the callback.</param>
        /// <param name="pad">Controller to halt vibration on.</param>
        private static void haltVibeCB(object timer, EventArgs e, Controller pad)
        {
            ((Timer)timer).Stop();
            Vibration vib = new Vibration();
            vib.LeftMotorSpeed = 0;
            vib.RightMotorSpeed = 0;
            pad.SetVibration(vib);
        }
    }
}
